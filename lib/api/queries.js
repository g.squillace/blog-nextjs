const imgFrag = `
  fragment imgFrag on ResponsiveImage {
    aspectRatio
    base64
    height
    sizes
    src
    srcSet
    webpSrcSet
    width
    alt
    title
  }
`;

export const getTestOnImage = `
  query homePage {
    homePage {
      imageCase1: image {
        responsiveImage(sizes: "(min-width: 1024px) 50vw, 100vw", imgixParams: {auto: [format, compress]}){
          ...imgFrag
        }
      }
      imageCase2: image {
        responsiveImage(sizes: "100vw", imgixParams: {auto: [format, compress]}){
          ...imgFrag
        }
      }
      thumbsImage: image {
        responsiveImage(sizes: "25vw, 300px", imgixParams: {auto: [format, compress], fit: crop, w:170, ar: "1:1"}){
          ...imgFrag
        }
      }
      mobileImage: image {
        responsiveImage(sizes: "100vw, 600px", imgixParams: {auto: [format, compress], fit: crop, ar: "1:3"}){
          ...imgFrag
        }
      }
      deskImage: image {
        responsiveImage(sizes: "50vw, 600px", imgixParams: {auto: [format, compress], fit: crop, ar: "16:9"}){
          ...imgFrag
        }
      }
    }
  }
  ${imgFrag}
`;