import Nav from './Nav';
import Footer from './Footer';

function Layout({ children }) {
  return (
    <div>
      <Nav />
      <div className="container">{children}</div>
      <Footer />
    </div>
  );
}

export default Layout;
