import Link from 'next/link';

var currentYear= new Date().getFullYear();


export default function Footer() {
  return (
    <footer className="footer">
      © Copy {currentYear} -
      <Link href="https://cantiere.agency.it" key="Credits">
        <a className="nav__item">
          Credits
        </a>
      </Link>
    </footer>
  )
}
