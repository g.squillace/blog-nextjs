import { Image } from 'react-datocms';
import * as queries from 'lib/api/queries';
import fetchDato from 'lib/api/dato';

function blockImage({ data }) {
  const { imageCase1, imageCase2, thumbsImage } = data;
  return (
    <div className="container mx-auto">
      <h2 className="text-center mt-8">Caso 1: Un&apos;immagine 100% su mobile, 50% del desktop (viewport). Senza crop, altezza libera in base alle proporzioni intrinseche dell&apos;immagine.</h2>
      <div className="lg:w-2/4 my-8 mx-auto">
        <Image data={imageCase1.responsiveImage} alt={imageCase1.alt} title={imageCase1.title}/>
      </div>
      <h2 className="text-center mt-16">Caso 2: Cover 100% width del contenitore (il viewport o contenitore), l&apos;altezza decisa da CSS e usata come object-fit.</h2>
      <Image className="image__cover h-80 relative w-full my-8 mx-auto" data={imageCase2.responsiveImage} alt={imageCase1.alt} title={imageCase1.title}/>
      <h2 className="text-center mt-16">Caso 3: chiamare due volte un immagine tramite alias, per ottenere con parametri diversi. Utile per Art Direction e thumbs di una gallery.</h2>
      <div className="lg:w-2/4 my-8 mx-auto">
        <Image className="mb-4" data={imageCase1.responsiveImage} alt={imageCase1.alt} title={imageCase1.title}/>
        <div className="flex justify-between">
          <Image data={thumbsImage.responsiveImage} alt={thumbsImage.alt} title={thumbsImage.title}/>
          <Image data={thumbsImage.responsiveImage} alt={thumbsImage.alt} title={thumbsImage.title}/>
          <Image data={thumbsImage.responsiveImage} alt={thumbsImage.alt} title={thumbsImage.title}/>
          <Image data={thumbsImage.responsiveImage} alt={thumbsImage.alt} title={thumbsImage.title}/>
        </div>
      </div>
      {/* <h2 className="text-center mt-16">Caso 4: chiamare due volte un immagine tramite alias, per ottenere con parametri diversi. Utile per Art Direction e thumbs di una gallery.</h2> */}
      <div></div>
    </div>
  )
}

export async function getStaticProps() {
  const response = await fetchDato(queries.getTestOnImage);
  const data = response.homePage;
  return {
    props: {
      data
    },
  };
}

export default blockImage;